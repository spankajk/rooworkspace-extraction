
#include "RooWorkspace.h"
#include "RooWorkspaceHandle.h"
#include "RooAbsPdf.h"
#include "RooRealVar.h"
#include "RooCategory.h"
#include "RooAbsData.h"
#include "RooCmdConfig.h"
#include "RooMsgService.h"
#include "RooConstVar.h"
#include "RooResolutionModel.h"
#include "RooPlot.h"
#include "RooRandom.h"
#include "TBuffer.h"
#include "TInterpreter.h"
#include "TClassTable.h"
#include "TBaseClass.h"
#include "TSystem.h"
#include "TRegexp.h"
#include "RooFactoryWSTool.h"
#include "RooAbsStudy.h"
#include "RooTObjWrap.h"
#include "RooAbsOptTestStatistic.h"
#include "TROOT.h"
#include "TFile.h"
#include "TH1.h"
#include "TClass.h"
#include "strlcpy.h"
 
// #include "ROOT/StringUtils.hxx"
 
#include <map>
#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include <cstring>
#include <list>

// OBSERVABLES = ["msd"]

// POIS = ["r","r_gghh","r_qqhh,kl","kt","CV","C2V"]

// PDFS = ["pdf_binfitfail", "pdf_binSRBin1", "pdf_binSRBin2", "pdf_binSRBin3"]


void get_limits() {
    std::string poi = "r,r_gghh,r_qqhh,kl,kt,CV,C2V";
    std::string pdfs = "pdf_binfitfail,pdf_binSRBin1,pdf_binSRBin2,pdf_binSRBin3";

    std::unique_ptr<TFile> myFile( TFile::Open("HHModel_model_all.root") );
    std::unique_ptr<RooWorkspace> w(myFile->Get<RooWorkspace>("w"));

    // Split the strings into lists
    std::vector<std::string> poi_lst, pdf_lst;
    std::string delimiter = ",";
    size_t pos = 0;

    while ((pos = poi.find(delimiter)) != std::string::npos) {
        std::string token = poi.substr(0, pos);
        poi_lst.push_back(token);
        poi.erase(0, pos + delimiter.length());
    }
    poi_lst.push_back(poi); // Add the last element

    while ((pos = pdfs.find(delimiter)) != std::string::npos) {
        std::string token = pdfs.substr(0, pos);
        pdf_lst.push_back(token);
        pdfs.erase(0, pos + delimiter.length());
    }
    pdf_lst.push_back(pdfs); // Add the last element

    // std::ofstream output("var_limits.txt");
    // if (!output.is_open()) {
    //     std::cerr << "Error opening the file." << std::endl;
    //     return 1;
    // }

    std::cout << "Limits for the variables" << std::endl;
    std::cout << "----------------------------------------------------" << std::endl;

    std::cout << "Limits for pois" << std::endl;
    for (const std::string& poi : poi_lst) {
        // cout << poi << std::endl;
		w->var(poi.c_str())->Print();
    }

    std::cout << "Values of pdfs" << std::endl;
    for (const std::string& pdf : pdf_lst) {
        // cout << w->pdf(pdf) << std::endl;
		w->pdf(pdfs.c_str())->Print();
    }

    // output.close();
}
