`DataExtract.C` is the latest version that extracts the data from the workspace that is created (datacards/ggf_hh4b_boosted/v9/HHModel_model_all.root). It outputs the relevant data into the `out.txt` file, which can then be parsed by our zfit program to find the relevant details to reimplement the analysis. 

`Data_Extraction.cpp` contains notes about the general workspace and functions that are useful in ROOT regarding the same.

`printSummary.C` is a preliminary program to print out some of the channel data. 

`get_limits.py` should work to also get the limits of the variables (following the syntax detailed in the ROOT tutorials), but doesn't seem to work from the actual terminal itself. 

`zfit_modelling_HH.py` has the zfit model that reads out parameters and limits from `out.txt` and directly implements them into zfit. It also builds the rest of the model. The most updated version is in the Google Colab at this link: https://colab.research.google.com/drive/1YjJdlm078zo5pvNB0uGsiy9av2JyBLwH?usp=sharing

`create_datacard.py` is the original script to build the HH model that is printed out in the datacard. 

`HHModel_model_all.txt` is the datacard itself. 
